import sys
import math
import random
from io import BytesIO
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.html import escape, mark_safe
from model_utils.managers import InheritanceManager
from django.utils.timezone import utc
from .validators import validate_file_extension

# Create your models here..


class User(AbstractUser):
    is_administrador = models.BooleanField(default=False)
    is_coordinacion = models.BooleanField(default=False)
    is_rector = models.BooleanField(default=False)
    is_psicologa = models.BooleanField(default=False)


class Aspectos_generales(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name

class Competencias(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name
    
class Criterios(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name

class Region(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name

class Comuna(models.Model):
    region = models.ForeignKey(Region,on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


def colegios_images(instance, filename):
    return 'UserImages/user_{0}/{1}'.format(instance.user.id, filename)

class Colegios(models.Model):
    name = models.CharField(max_length=250)
    imagen = models.ImageField( null = True, upload_to='colegios_images',default=False, blank=True,verbose_name=('Imagen'), unique=True)

    def __str__(self):
        return self.name

class Colegios2(models.Model):
    name = models.CharField(max_length=250)
    imagen = models.ImageField( null = True, upload_to='colegios_images',default=False, blank=True,verbose_name=('Imagen'), unique=True)

    def __str__(self):
        return self.name

class Colegios3(models.Model):
    name = models.CharField(max_length=250)
    imagen = models.ImageField( null = True, upload_to='colegios_images',default=False, blank=True,verbose_name=('Imagen'), unique=True)

    def __str__(self):
        return self.name

class Cargo(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Disposicion(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Titulo(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Ingles(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Años(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Nivel_ingles(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name
    
class Nivel_Tec(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Coordinacion(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    colegio = models.ManyToManyField(Colegios, related_name='colegio_coordinacion', blank=True)

    def __str__(self):
        return self.user.first_name

class Rector(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    colegio = models.ManyToManyField(Colegios, related_name='colegio_rector', blank=True)
    def __str__(self):
        return self.user.first_name

class Psicologa(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    colegio = models.ManyToManyField(Colegios, related_name='colegio_psicologa', blank=True)
    def __str__(self):
        return self.user.first_name

class Postulacion(models.Model):
   nombre = models.CharField(max_length=250)
   apellido_p = models.CharField(max_length=250, verbose_name=('Apellido Paterno')) 
   apellido_m = models.CharField(max_length=250, verbose_name=('Apellido Materno'))
   rut = models.CharField(max_length=250)
   telefono = models.CharField(max_length=250, verbose_name=('Teléfono'), help_text=("El formato debe ser el siguiente +569xxxxxxxx"))
   email = models.EmailField(verbose_name=("Correo"))
   cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE, help_text='Cargo al que postula')
   colegio1 = models.ForeignKey(Colegios, on_delete=models.CASCADE, help_text=('Colegio al que postula'), verbose_name=('Preferencia 1'))
   colegio2 = models.ForeignKey(Colegios2, on_delete=models.CASCADE, help_text=('Colegio al que postula'), verbose_name=('Preferencia 2'), null=True, blank=True)
   colegio3 = models.ForeignKey(Colegios3, on_delete=models.CASCADE, help_text=('Colegio al que postula'), verbose_name=('Preferencia 3'), null=True, blank=True)
   años = models.ForeignKey(Años, on_delete=models.CASCADE, verbose_name=('Años de experiencia'))
   cv = models.FileField(null=False, upload_to='cv', blank=False,verbose_name=('Adjuntar CV'), validators=[validate_file_extension], help_text='Solo se permiten archivos PDF')
   otros_documentos = models.FileField(null=True, upload_to='cv', blank=True, verbose_name=('Otros Documentos'),validators=[validate_file_extension], help_text='Solo se permiten archivos PDF')
   ficha_fat = models.FileField(null=True, upload_to='FAT', blank=True,verbose_name=('Adjuntar Ficha FAT'))
   disponibilidad = models.ManyToManyField(Disposicion, related_name='disponibilidad', help_text=('Puede escoger más de una.'))
   titulo = models.ForeignKey(Titulo, on_delete=models.CASCADE, verbose_name=('Título'))
   nivel_ingles = models.ForeignKey(Nivel_ingles, on_delete=models.CASCADE, verbose_name=('Nivel De Ingles'))
   valido = models.BooleanField(null= False, default=False, verbose_name=('Aprobar'))
   email_send = models.BooleanField(null= False, default=False, verbose_name=('Email_enviado'))
   send = models.BooleanField(null= False, default=False, verbose_name=('Email_enviado_monica'))
   rechazar = models.BooleanField(null= False, default=False, verbose_name=('Rechazar Psicolaboral'))
   rechazar_rector = models.BooleanField(null= False, default=False, verbose_name=('Rechazar Rector'))
   rechazar_coordinador = models.BooleanField(null= False, default=False, verbose_name=('Rechazar Coordinador'))

   aceptada = models.BooleanField(null= False, default=False, verbose_name=('Aceptada'))
   rechazada = models.BooleanField(null= False, default=False, verbose_name=('Rechazada'))
   Siguiente = models.BooleanField(null= False, default=False, verbose_name=('Entrevistar'))
   liberar = models.BooleanField(null= False, default=False, verbose_name=('Liberar para todos'))
   aprobar_demos = models.BooleanField(null=False, default=False, verbose_name=('Aprobar'))
   rechazar_demos = models.BooleanField(null=False, default=False, verbose_name=('No aprobar'))
   entrevistado = models.BooleanField(null= False,default=False ,verbose_name=('Entrevistado'))
   entrevistado_demo = models.BooleanField(null= False,default=False ,verbose_name=('Entrevistado_demo'))
   psicologa = models.BooleanField(null= False,default=False, verbose_name=("Aprobar psicolaboral"))
   nivel_ingles = models.ForeignKey(Nivel_ingles, on_delete=models.CASCADE, verbose_name=("Nivel Inglés"))
   nivel_tec = models.ManyToManyField(Nivel_Tec, verbose_name=("Dominio de herramientas tecnológicas"), help_text=('Puede escoger más de una.'))
   enviada = models.DateField(auto_now_add=True)
   nombre_r = models.CharField(max_length=250, verbose_name=("Nombre"))
   cargo_r = models.CharField(max_length=250, verbose_name=("Cargo"))
   corre_r = models.EmailField(verbose_name=("Correo"))
   telefono_r = models.CharField(max_length=250, verbose_name=("Teléfono"), help_text=("El formato debe ser el siguiente +569xxxxxxxx"))
   relacion_r = models.CharField(max_length=250, verbose_name=("Relación laboral"))
   instituto_r = models.CharField(max_length=250, verbose_name=("Institución"))
   nombre_r_2 = models.CharField(max_length=250, verbose_name=("Nombre"))
   cargo_r_2 = models.CharField(max_length=250, verbose_name=("Cargo"))
   corre_r_2 = models.EmailField(verbose_name=("Correo"))
   telefono_r_2 = models.CharField(max_length=250, verbose_name=("Teléfono"), help_text=("El formato debe ser el siguiente +569xxxxxxxx"))
   relacion_r_2 = models.CharField(max_length=250, verbose_name=("Relación laboral"))
   instituto_r_2 = models.CharField(max_length=250, verbose_name=("Institución"))
   rector = models.ForeignKey(User, on_delete=models.CASCADE, related_name='rector_cargo', null=True, blank=True)
   
   def __str__(self):
       return self.nombre




class Rubrica(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Owner_postulacion')
    postulacion = models.ForeignKey(Postulacion, on_delete=models.CASCADE, related_name='postulacion')
    entrevistado = models.BooleanField(null= False, default=True, verbose_name=('Entrevistado'))
    puntos = models.IntegerField()
    puntos2 = models.IntegerField(verbose_name=('Puntos'))
    puntos3 = models.IntegerField(verbose_name=('Puntos'))
    puntos4 = models.IntegerField(verbose_name=('Puntos'))
    puntos5 = models.IntegerField(verbose_name=('Puntos'))
    puntos6 = models.IntegerField(verbose_name=('Puntos'))
    puntos7 = models.IntegerField(verbose_name=('Puntos'))
    puntos8 = models.IntegerField(verbose_name=('Puntos'))
    puntos9 = models.IntegerField(verbose_name=('Puntos'))
    puntos10 = models.IntegerField(verbose_name=('Puntos'))
    comentario = models.TextField(max_length=1000)
    comentario2 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario3 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario4 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario5 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario6 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario7 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario8 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario9 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario10 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    observaciones = models.CharField(max_length=1000, verbose_name=(''), null=True, blank=True)


    def __str__(self):
        return self.postulacion.nombre

class Rubrica_coordinador(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Owner_postulacion_c')
    postulacion = models.ForeignKey(Postulacion, on_delete=models.CASCADE, related_name='postulacion_c')
    entrevistado = models.BooleanField(null= False, default=True, verbose_name=('Entrevistado_c'))
    puntos = models.IntegerField()
    puntos2 = models.IntegerField(verbose_name=('Puntos'))
    puntos3 = models.IntegerField(verbose_name=('Puntos'))
    puntos4 = models.IntegerField(verbose_name=('Puntos'))
    puntos5 = models.IntegerField(verbose_name=('Puntos'))
    puntos6 = models.IntegerField(verbose_name=('Puntos'))
    puntos7 = models.IntegerField(verbose_name=('Puntos'))

    comentario = models.TextField(max_length=1000)
    comentario2 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario3 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario4 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario5 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario6 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario7 = models.TextField(max_length=1000, verbose_name=('Comentario'))

    observaciones = models.CharField(max_length=1000, verbose_name=(''), null=True, blank=True)


    def __str__(self):
        return self.postulacion.nombre


class Rubrica_equipomulti(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Owner_postulacion_em')
    postulacion = models.ForeignKey(Postulacion, on_delete=models.CASCADE, related_name='postulacion_em')
    entrevistado = models.BooleanField(null= False, default=True, verbose_name=('Entrevistado'))
    puntos = models.IntegerField()
    puntos2 = models.IntegerField(verbose_name=('Puntos'))
    puntos3 = models.IntegerField(verbose_name=('Puntos'))
    puntos4 = models.IntegerField(verbose_name=('Puntos'))
    puntos5 = models.IntegerField(verbose_name=('Puntos'))
    puntos6 = models.IntegerField(verbose_name=('Puntos'))
    puntos7 = models.IntegerField(verbose_name=('Puntos'))
    puntos8 = models.IntegerField(verbose_name=('Puntos'))
    puntos9 = models.IntegerField(verbose_name=('Puntos'))

    comentario = models.TextField(max_length=1000)
    comentario2 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario3 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario4 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario5 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario6 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario7 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario8 = models.TextField(max_length=1000, verbose_name=('Comentario'))
    comentario9 = models.TextField(max_length=1000, verbose_name=('Comentario'))

    observaciones = models.CharField(max_length=1000, verbose_name=(''), null=True, blank=True)


    def __str__(self):
        return self.postulacion.nombre


OPCIONES =(
    ("1", "Bajo lo esperado"),
    ("2","Adecuado"),
    ("3","Sobre lo esperado")
)

class Conclusion(models.Model):
    name = models.CharField(max_length=250)
    
    def __str__(self):
        return self.name

class Rubrica_psicologa(models.Model):
    postulantes = models.ForeignKey(Postulacion, on_delete=models.CASCADE, related_name='postulantes')
    aspecto = models.ForeignKey(Aspectos_generales, on_delete=models.CASCADE, verbose_name=(''),related_name=('aspectos_1'))
    aspecto_2 = models.ForeignKey(Aspectos_generales, on_delete=models.CASCADE, verbose_name=(''))
    competencia_1 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_1'))
    competencia_2 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_2'))
    competencia_3 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_3'))
    competencia_4 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_4'))
    competencia_5 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_5'))
    competencia_6 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_6'))
    competencia_7 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_7'))
    competencia_8 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_8'))
    competencia_9 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_9'))
    competencia_10 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_10'))
    competencia_11 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_11'))
    competencia_12 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_12'))
    competencia_13 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_13'))
    competencia_14 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_14'))
    competencia_15 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_15'))
    competencia_16 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_16'))
    competencia_17 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_17'))
    competencia_18 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_18'))
    competencia_19 = models.ForeignKey(Competencias, on_delete=models.CASCADE, verbose_name=(''), related_name=('competencia_19'))
    conclusion = models.ForeignKey(Conclusion, on_delete=models.CASCADE, verbose_name=(''))
    fortalezas = models.CharField(max_length=1000, verbose_name=(''))
    fortalezas2 = models.CharField(max_length=1000, verbose_name=(''))
    fortalezas3 = models.CharField(max_length=1000, verbose_name=(''))
    posibilidades = models.CharField(max_length=1000, verbose_name=(''))
    posibilidades2 = models.CharField(max_length=1000, verbose_name=(''))
    posibilidades3 = models.CharField(max_length=1000, verbose_name=(''))
    sugerencias = models.TextField(max_length=1000, verbose_name=(''))

class Rubrica_demo(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Owner_postulacion_demo')
    postulacion = models.ForeignKey(Postulacion, on_delete=models.CASCADE, related_name='postulacion_demo')
    criterio_1 = models.ForeignKey(Criterios, on_delete=models.CASCADE, related_name=('criterio_1'))
    criterio_2 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_2'))
    criterio_3 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_3'))
    criterio_4 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_4'))
    criterio_5 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_5'))
    criterio_6 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_6'))
    criterio_7 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_7'))
    criterio_8 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_8'))
    criterio_9 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_9'))
    criterio_10 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_10'))
    criterio_11 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_11'))
    criterio_12 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_12'))
    criterio_13 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_13'))
    criterio_14 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_14'))
    criterio_15 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_15'))
    criterio_16 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_16'))
    criterio_17 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_17'))
    criterio_18 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_18'))
    criterio_19 = models.ForeignKey(Criterios, on_delete=models.CASCADE,related_name=('criterio_19'))
    observaciones = models.CharField(max_length=1000, verbose_name=(''), null=True, blank=True)
    
    def __str__(self):
        return self.postulacion.nombre
