from django.urls import include, path
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from .views import cv, coordinacion, rectores, psicologa
urlpatterns = [
    path('postulacion', cv.index, name='index'),
    path('', cv.home, name='home'),
    path('user_profile', cv.user_profile, name='user'),
    path('coordinacion/', include(([
        path('postulantes', coordinacion.index, name='index'),
        path('rechazados', coordinacion.rechazados, name='rechazados'),
        path('finalizado', coordinacion.finalizados, name='fin'),
        path('finalizados', coordinacion.finalizados_c, name='fin_c'),

        path('coordinadores', coordinacion.index_c, name='coordinadores'),
        path('coordinadores/entrevistar', coordinacion.entrevistar_c, name='entrevistar'),
        path('coordinadores/<int:pk>/rubrica/', coordinacion.rubrica, name='rubrica'),
        path('postulantes/<int:pk>/rubricas/', coordinacion.rubricas, name='rubricas'),

        path('postulantes/<int:pk>/rubrica_rector/', coordinacion.vista_rubrica, name='vista_rubrica'),
        path('coordinadores/<int:pk>/rubrica_coordinador/', coordinacion.vista_rubricaC, name='vista_rubrica_coordinador'),
        path('postulantes/<int:pk>/vista_rubrica_equipo/', coordinacion.vista_rubrica_equipo, name='vista_rubrica_equipo'),
        path('postulantes/<int:pk>/vista_rubrica_psicologa/', coordinacion.vista_rubrica_psicologa, name='vista_rubrica_psicologa'),
        path('postulantes/<int:pk>/vista_rubrica_demo/', coordinacion.vista_rubrica_demo, name='vista_rubrica_demo'),
        path('activos', coordinacion.activos, name='activos'),
        path('proceso', coordinacion.proceso, name='proceso'),
        path('psicolaboral', coordinacion.psicolaboral, name='psicolaboral'),
        path('psicolaboral_coordinador', coordinacion.psicolaboral_c, name='psicolaboral_c'),

        path('postulantes/<int:pk>/', coordinacion.info, name='info'),
        path('postulantes/<int:pk>/delete', coordinacion.PostulanteDeleteView.as_view(), name='borrar'),
    ], 'CV_APP'), namespace='coordinacion')),

    path('rectores/', include(([
        path('postulantes', rectores.index, name='index'),
        path('postulantes/<int:pk>/', rectores.info, name='info'),
        path('postulantes/<int:pk>/', rectores.info2, name='informacion2'),
        path('postulantes/rechazados/', rectores.cargo, name='rechazados'),
        path('postulantes/clasedemo/', rectores.clasedemo, name='clasedemo'),
        path('postulantes/oferta/', rectores.oferta_view, name='oferta_view'),

        path('postulantes/entrevistas/', rectores.entrevista, name='entrevistas'),
        path('postulantes/<int:pk>/rubricas/', rectores.rubricas, name='rubricas'),
        path('postulantes/<int:pk>/rubrica/', rectores.rubrica, name='rubrica'),
        path('postulantes/<int:pk>/vista_rubrica/', rectores.vista_rubrica, name='vista_rubrica'),
        path('postulantes/<int:pk>/vista_rubrica_equipo/', rectores.vista_rubrica_equipo, name='vista_rubrica_equipo'),
        path('postulantes/<int:pk>/vista_rubrica_demo/', rectores.vista_rubrica_demo, name='vista_rubrica_demo'),
        path('postulantes/<int:pk>/vista_rubrica_psicologa/', rectores.vista_rubrica_psicologa, name='vista_rubrica_psicologa'),
        path('postulantes/<int:pk>/rubrica_equipo/', rectores.rubricaem, name='rubrica_em'),
        path('postulantes/<int:pk>/rubrica_demo/', rectores.rubricademo, name='rubrica_demo'),
        path('postulantes/<int:pk>/send/', rectores.send, name='send'),
        path('postulantes/<int:pk>/reportar/', rectores.reportar, name='reportar'),
        path('postulantes/<int:pk>/ficha/', rectores.subirficha, name='ficha'),
        path('postulantes/<int:pk>/oferta_laboral/', rectores.oferta, name='oferta'),
        path('finalizado', rectores.finalizados, name='fin'),
        path('postulantes/aprobados/',rectores.aprobados, name='aprobados'),
    ], 'CV_APP'), namespace='rectores')),

    path('psicologa/', include(([
        path('postulantes', psicologa.index, name='index'),
        path('postulantes/<int:pk>/vista_rubrica/', psicologa.vista_rubrica, name='vista_rubrica'),
        path('postulantes/<int:pk>/rubrica_coordinador/',psicologa.vista_rubricaC, name='vista_rubrica_coordinador'),
        path('postulantes/<int:pk>/rubrica_psicologica/', psicologa.rubrica, name='rubrica'),
        path('postulantes/entrevistados/',psicologa.entrevistados, name='entrevistados'),
        path('postulantes/<int:pk>/vista_rubrica_psicologa/', psicologa.vista_rubrica_psicologa, name='vista_rubrica_psicologa'),
        path('postulantes/<int:pk>/rubricas/', psicologa.rubricas, name='rubricas'),
        path('postulantes/<int:pk>/vista_rubrica_equipo/', psicologa.vista_rubrica_equipo, name='vista_rubrica_equipo'),
        path('postulantes/<int:pk>/vista_rubrica_demo/', psicologa.vista_rubrica_demo, name='vista_rubrica_demo'),
    ], 'CV_APP'), namespace='psicologa')),
    

]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)