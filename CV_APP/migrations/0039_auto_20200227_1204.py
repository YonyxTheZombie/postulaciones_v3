# Generated by Django 2.2.7 on 2020-02-27 15:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0038_auto_20200226_1321'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postulacion',
            name='entrevistado',
            field=models.BooleanField(verbose_name='Entrevistado'),
        ),
    ]
