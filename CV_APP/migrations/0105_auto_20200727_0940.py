# Generated by Django 2.2.7 on 2020-07-27 13:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0104_postulacion_send'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postulacion',
            name='relacion_r',
            field=models.CharField(max_length=250, verbose_name='Relación Laboral'),
        ),
        migrations.AlterField(
            model_name='postulacion',
            name='relacion_r_2',
            field=models.CharField(max_length=250, verbose_name='Relación Laboral'),
        ),
    ]
