# Generated by Django 2.2.7 on 2020-08-14 07:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0114_postulacion_rector'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postulacion',
            name='rector',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='rector_cargo', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
