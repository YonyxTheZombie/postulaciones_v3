# Generated by Django 2.2.7 on 2020-02-25 15:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0030_auto_20200225_0834'),
    ]

    operations = [
        migrations.AddField(
            model_name='rubrica',
            name='entrevistado',
            field=models.BooleanField(default=False, verbose_name='Entrevistado'),
        ),
    ]
