# Generated by Django 2.2.7 on 2020-08-14 07:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0113_auto_20200814_0051'),
    ]

    operations = [
        migrations.AddField(
            model_name='postulacion',
            name='rector',
            field=models.CharField(blank=True, max_length=250, null=True, verbose_name='Rector'),
        ),
    ]
