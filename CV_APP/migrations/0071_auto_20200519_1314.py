# Generated by Django 2.2.7 on 2020-05-19 17:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0070_auto_20200514_1525'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postulacion',
            name='psicologa',
            field=models.BooleanField(default=False, verbose_name='Aprobar psicolaboral'),
        ),
    ]
