# Generated by Django 2.2.7 on 2020-05-08 13:31

import CV_APP.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0062_auto_20200508_0921'),
    ]

    operations = [
        migrations.AddField(
            model_name='postulacion',
            name='otros_documentos',
            field=models.FileField(blank=True, help_text='Solo se permiten archivos PDF', null=True, upload_to='cv', validators=[CV_APP.validators.validate_file_extension], verbose_name='Otros Documentos'),
        ),
    ]
