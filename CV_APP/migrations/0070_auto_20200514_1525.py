# Generated by Django 2.2.7 on 2020-05-14 19:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0069_postulacion_titulo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postulacion',
            name='titulo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='CV_APP.Titulo', verbose_name='Titulo'),
        ),
    ]
