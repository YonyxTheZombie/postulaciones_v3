# Generated by Django 2.2.7 on 2020-01-22 15:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0006_auto_20200122_1006'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postulacion',
            name='años',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='CV_APP.Años', verbose_name='Años de experiencia'),
        ),
    ]
