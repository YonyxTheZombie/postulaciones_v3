# Generated by Django 2.2.7 on 2020-07-20 14:53

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0093_rubrica_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='rubrica_coordinador',
            name='owner',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='Owner_postulacion_c', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_equipomulti',
            name='owner',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='Owner_postulacion_em', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
