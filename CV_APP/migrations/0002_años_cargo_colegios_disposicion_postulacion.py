# Generated by Django 2.2.7 on 2020-01-21 18:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Años',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Cargo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Colegios',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('imagen', models.ImageField(blank=True, default=False, null=True, unique=True, upload_to='colegios_images', verbose_name='Imagen')),
            ],
        ),
        migrations.CreateModel(
            name='Disposicion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Postulacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
                ('apellido_p', models.CharField(max_length=250)),
                ('apellido_m', models.CharField(max_length=250)),
                ('rut', models.CharField(max_length=250)),
                ('telefono', models.CharField(max_length=250)),
                ('email', models.EmailField(max_length=254)),
                ('cv', models.FileField(upload_to='cv', verbose_name='CV')),
                ('años', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='CV_APP.Años')),
                ('cargo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='CV_APP.Cargo')),
                ('colegio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='CV_APP.Colegios')),
                ('disponibilidad', models.ManyToManyField(to='CV_APP.Disposicion')),
            ],
        ),
    ]
