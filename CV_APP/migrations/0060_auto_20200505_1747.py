# Generated by Django 2.2.7 on 2020-05-05 21:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CV_APP', '0059_rubrica_psicologa_competencia_1'),
    ]

    operations = [
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_10',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_10', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_11',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_11', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_12',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_12', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_13',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_13', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_14',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_14', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_15',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_15', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_16',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_16', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_17',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_17', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_18',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_18', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_19',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_19', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_2',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_2', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_3',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_3', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_4',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_4', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_5',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_5', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_6',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_6', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_7',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_7', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_8',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_8', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rubrica_psicologa',
            name='competencia_9',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='competencia_9', to='CV_APP.Competencias', verbose_name=''),
            preserve_default=False,
        ),
    ]
